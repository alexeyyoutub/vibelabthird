package Third;

public class ThirdWhile {
    public static void main(String[] args) {
        int x = 1, y = 1, k, i = 2;
        System.out.print(x +" "+ y);
        while (i < 11)
        {
            k = y;
            y = x + y;
            x = k;
            System.out.print(" "+ y);
            i++;
        }
    }
}
